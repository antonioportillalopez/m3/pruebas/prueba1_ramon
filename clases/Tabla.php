<?php



namespace clases;

class Tabla {
    public $numero;
    
    public function __construct($n) {
        $this->numero=$n;
    }
    
    public function dibujar() {
        $a="";

        for($c=0;$c<$this->numero;$c++){
            //$a.=require './clases/vistas/tabla_variable.php';
            //$a.= str_replace("{{c}}", $c, file_get_contents('./clases/vistas/tabla.php'));
            ob_start();
            require './clases/vistas/tabla_php_html.php';
            $a.=ob_get_clean();
        }
        
        return $a;
    }
}
