<?php
namespace controladores;

class siteController extends Controller{
    private $miPie;
    private $miMenu;
        
    public function __construct() {
        parent::__construct();
        $this->miPie="Prueba de clase.<br>Autor Ramon Abramo";
        $this->miMenu=[
            "Inicio"=>$this->crearRuta(["accion"=>"index"])                
        ];
    }
    
    /**
     * cuando carga la web
     */
    public function indexAccion(){
      $this->render([
          "vista"=>"index",
          "pie"=>$this->miPie,
          "menu"=>(new \clases\Menu($this->miMenu,"Inicio"))->html()
    ]);
    }
    
    /**
     * cuando pulsa
     */
    public function paso2Accion($p) {
        $resultado=new \clases\Tabla($p->getValores()["numero"]);
        $this->render([
          "vista"=>"paso2",
          "pie"=>$this->miPie,
          "menu"=>(new \clases\Menu($this->miMenu,"Inicio"))->html(),
          "resultado"=>$resultado->dibujar(),  
    ]);
        
    }
    
}
